public class Jackpot{
	public static void main(String[] args){
		System.out.println("Welcome!");
		Board board = new Board();
        Die dice = new Die();
		boolean gameOver = false;
		int numOfTilesClosed = 0;

		while(!gameOver){
			System.out.println(board);

			if(board.playATurn())
				gameOver = true;
			else
				numOfTilesClosed = 1;
		}

		if(numOfTilesClosed >= 7)
			System.out.println("The player reached the jackpot and won!");
		else
			System.out.println("The player did not reach the jackpot and lost");	
	}
}
